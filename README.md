# Bitville-app

## Description

SPIN 'N' WIN is a game for testing one's reaction by spinning numbers faster with every win aka the new level. Maximum level is 10. The least tries you take to reach maximum level, the better your reaction is!

## Technologies used

Vue.js, Webpack

## Availability

The app is available to visit at:

GitLab Pages: https://jassblack.gitlab.io/bitville-app/

Public URL: http://35.157.126.107/

## Install & Run

``` bash
# clone the repo
git clone git@gitlab.com:JassBlack/bitville-app.git

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

